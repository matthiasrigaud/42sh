##
## Makefile for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh
##
## Made by Matthias RIGAUD
## Login   <rigaud_b@epitech.net>
##
## Started on  Mon May 30 17:54:23 2016 Matthias RIGAUD
## Last update Sun Jun  5 00:43:51 2016 Matthias RIGAUD
##

RM		=	\rm -f

CC		=	gcc

NAME		= 	42sh

SRCS		= 	srcs/main.c 		\
			srcs/div.c 		\
			srcs/get_next_line.c 	\
			srcs/command.c 		\
			srcs/cd.c 		\
			srcs/env.c 		\
			srcs/redirect_out.c 	\
			srcs/exit.c 		\
			srcs/dollar.c 		\
			srcs/tab_utilities.c 	\
			srcs/sig_catch.c 	\
			srcs/prep_str.c 	\
			srcs/exec_ext.c 	\
			srcs/redirect_in.c 	\
			srcs/free.c		\
			srcs/prompt.c		\
			srcs/delete.c		\
			srcs/rc_reader.c	\
			srcs/free_buffer.c	\
			srcs/builtin.c		\
			srcs/alias.c		\
			srcs/echo.c		\
			srcs/background.c	\
			srcs/replace_alias.c	\
			srcs/alias_list.c	\
			srcs/builtin2.c		\

OBJS		= 	$(SRCS:.c=.o)

SHEETIES	= 	$(OBJS)		\
		  	$(SRCS:.c=.c~)	\
		  	Makefile~	\

CFLAGS		= 	-W -Wall -Werror -Wextra -ansi -pedantic -Iinclude

LIB		= 	-Llib -lmy

all		: 	lib $(NAME)

lib		:
		  	make -C lib/my

$(NAME)		: 	$(OBJS)
		  	@echo -e "\e[0m"
		  	@$(CC) $(OBJS) $(LIB) -o $(NAME)
		  	@echo -e "\e[32mAll done ! ==>\e[33m" $(NAME) "\e[32mcreated !\e[0m"

clean:
		  	make -C lib/my clean
		  	@echo -en "\e[0mCleaning .o && .c~ files..."
		  	@$(RM) $(SHEETIES)
		  	@echo -e "	 [\e[32mOk !\e[0m]"

fclean: 		clean
		  	make -C lib/my fclean
		  	@echo -en "\e[39mCleaning executable..."
			@$(RM) $(NAME)
			@echo -e "		 [\e[32m0k !\e[0m]"

re:	 		fclean all

comp: 			re
			@echo -en "\e[0mCleaning .o && .c~ files..."
			@$(RM) $(SHEETIES)
			@echo -e "	 [\e[32mOk !\e[0m]"

.c.o		: 	%.c
			@$(CC) -c $< -o $@ $(CFLAGS) && \
			echo -e "\e[32m[OK]" $< "\e[93m"|| \
			echo -e "\e[91;5m[ERR]\e[25m" $< "\e[93m"

.PHONY		: 	all clean fclean re comp lib
