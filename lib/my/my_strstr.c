/*
** my_strstr.c for my_strstr in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 10:50:13 2015 Matthias RIGAUD
** Last update Wed Dec 16 14:19:38 2015 Matthias RIGAUD
*/

#include <stdlib.h>

int	testst(int *i, int *backup, int *j)
{
  if (*backup != 0)
    {
      *i = *backup;
    }
  *j = 0;
  *backup = 0;
  return (0);
}

char	*testt(int i[2], int backup, char *str, char *to_find)
{
  char	*returner;

  returner = NULL;
  while (str[i[0]] != 0)
    {
      if (str[i[0]] == to_find[i[1]])
        {
          if (to_find[i[1] + 1] == 0)
            {
              returner = &str[i[0] - i[1]];
              return (returner);
            }
          else if (str[i[0]] == to_find[0])
            {
              backup = i[0];
            }
          i[1] = i[1] + 1;
        }
      else
        {
          testst(&i[0], &backup, &i[1]);
        }
      i[0] = i[0] + 1;
    }
  return (returner);
}

char	*my_strstr(char *str, char *to_find)
{
  int	i[2];
  int	backup;

  i[0] = 0;
  i[1] = 0;
  backup = 0;
  if (to_find[0] == 0)
    {
      return (str);
    }
  return (testt(i, backup, str, to_find));
}
