/*
** my_strdup.c for my_strdup in /home/rigaud_b/rendu/j08
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Wed Oct  7 10:27:01 2015 Matthias RIGAUD
** Last update Tue Oct 27 13:48:43 2015 Matthias RIGAUD
*/

#include <stdlib.h>
#include <my.h>

char	*my_strdup(char *src)
{
  int	size;
  char	*dest;

  size = my_strlen(src);
  dest = malloc(size + 1);
  if (dest == NULL)
    {
      return (NULL);
    }
  dest = my_strcpy(dest, src);
  return (dest);
}
