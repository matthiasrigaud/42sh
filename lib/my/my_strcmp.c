/*
** my_strcmp.c for my_strcmp in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 11:20:45 2015 Matthias RIGAUD
** Last update Tue Jan 19 17:06:31 2016 Matthias RIGAUD
*/

#include <my.h>

int	my_strcmp(char *s1, char *s2)
{
  int	i;

  i = 0;
  if (!s2)
    return (-s1[0]);
  while (s1[i] != 0 && s2[i] != 0)
    {
      if (s1[i] != s2[i])
	{
	  return (s1[i] - s2[i]);
	}
      i = i + 1;
    }
  if (s1[i] != s2[i])
    {
      return (s1[i] - s2[i]);
    }
  return (0);
}
