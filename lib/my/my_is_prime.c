/*
** my_is_prime.c for my_is_prime in /home/rigaud_b/rendu/j05
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Fri Oct  2 22:37:10 2015 Matthias RIGAUD
** Last update Tue Oct  6 12:00:11 2015 Matthias RIGAUD
*/

int	my_is_prime(int nombre)
{
  int	iterator;

  if (nombre % 2 == 0)
    {
      return (0);
    }
  iterator = 3;
  while (iterator < nombre / 2)
    {
      if (nombre % iterator == 0)
	{
	  return (0);
	}
      iterator = iterator + 2;
    }
  return (1);
}
