/*
** my_char_isnum.c for my_char_num in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 14:05:47 2015 Matthias RIGAUD
** Last update Thu Oct  8 11:47:20 2015 Matthias RIGAUD
*/

int	my_char_isnum(char c)
{
  if (c >= 48 && c <= 57)
    {
      return (1);
    }
  else
    {
      return (0);
    }
}
