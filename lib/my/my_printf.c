/*
** my_printf.c for my_printf in /home/rigaud_b/rendu/PSU_2015_my_printf
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Fri Nov  6 11:05:05 2015 Matthias RIGAUD
** Last update Wed Nov 25 12:24:00 2015 Matthias RIGAUD
*/

#include <stdarg.h>
#include <my.h>

int	is_car_type(char c)
{
  if (c == 'd' ||
      c == 'i' ||
      c == 'o' ||
      c == 'u' ||
      c == 'x' ||
      c == 'X' ||
      c == 'c' ||
      c == 's' ||
      c == 'p' ||
      c == 'b' ||
      c == 'S' ||
      c == 'f' ||
      c == '%')
    {
      return (1);
    }
  return (0);
}

int	my_putstrspe(char *str)
{
  int	i;
  int	r;

  i = -1;
  r = 0;
  while (str[++i] != 0)
    {
      if (str[i] < 32 || str[i] > 126)
	{
	  r += my_putchar(92);
	  if (str[i] <= 7)
	    r += my_putstr("00\0");
	  else if (str[i] < 32)
	    r += my_putstr("0\0");
	  r += my_putnbr_base(str[i], "01234567");
	}
      else
	r += my_putchar(str[i]);
    }
  return (r);
}

int	my_displayor(va_list *l, char type, int r)
{
  if (type == 'p')
    r += my_putstr("0x\0");
  if (type == 'd' || type == 'i')
    r += my_put_nbr(va_arg(*l, int));
  if (type == 'o')
    r += my_putnbr_base_uns(va_arg(*l, unsigned int), "01234567");
  if (type == 'u')
    r += my_put_nbr_uns(va_arg(*l, unsigned int));
  if (type == 'x' || type == 'p')
    r += my_putnbr_base_uns(va_arg(*l, long unsigned int), "0123456789abcdef");
  if (type == 'X')
    r += my_putnbr_base_uns(va_arg(*l, unsigned int), "0123456789ABCDEF");
  if (type == 'c')
    r += my_putchar(va_arg(*l, int));
  if (type == 's')
    r += my_putstr(va_arg(*l, char*));
  if (type == 'b')
    r += my_putnbr_base_uns(va_arg(*l, unsigned int), "01");
  if (type == 'S')
    r += my_putstrspe(va_arg(*l, char*));
  if (type == '%')
    r += my_putchar('%');
  if (type == 'f')
    r += my_put_double(va_arg(*l, double));
  return (r);
}

int		my_printf(const char *format, ...)
{
  va_list	list;
  int		returner;
  int		i;

  va_start(list, format);
  i = -1;
  returner = 0;
  while (format[++i] != 0)
    {
      if (format[i] != '%')
	{
	  returner += my_putchar(format[i]);
	}
      else
	{
	  while (is_car_type(format[++i]) == 0)
	    {
	    }
	  returner += my_displayor(&list, format[i], 0);
	}
    }
  va_end(list);
  return (returner);
}
