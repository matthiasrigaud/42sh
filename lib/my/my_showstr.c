/*
** my_showstr.c for my_showstr in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Tue Oct  6 10:23:15 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:13:33 2015 matteo melis
*/

#include <my.h>

int	my_showstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    {
      if (str[i] >= 32 && str[i] <= 126)
	{
	  my_putchar(str[i]);
	}
      else
	{
	  my_putchar(92);
	  if (str[i] <= 15)
	    {
	      my_putchar(48);
	    }
	  my_putnbr_base(str[i], "0123456789abcdef");
	}
      i = i + 1;
    }
  return (0);
}
