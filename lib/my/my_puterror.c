/*
** my_puterror.c for libmy in /home/rigaud_b/rendu/103architect/lib/my
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
**
** Started on  Mon Nov 30 15:50:19 2015 Matthias RIGAUD
** Last update Wed Jun  1 21:44:46 2016 Matthias RIGAUD
*/

#include <stdlib.h>
#include <unistd.h>
#include <my.h>

int	my_puterror(char *cause, int ex)
{
  write(2, cause, my_strlen(cause));
  if (!ex)
    return (1);
  exit(ex);
}
