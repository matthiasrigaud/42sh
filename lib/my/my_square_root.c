/*
** my_square_root.c for my_square_root in /home/rigaud_b/rendu/Piscine_C_J05
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Fri Oct  2 19:29:08 2015 Matthias RIGAUD
** Last update Mon Oct 26 09:49:18 2015 matteo melis
*/

#include <my.h>

int	my_square_root(int nb)
{
  int	iterator;

  iterator = 1;
  if (nb <= 0)
    {
      return (0);
    }
  else if (nb == 1)
    {
      return (1);
    }
  while (iterator < nb /2)
    {
      if (my_power_rec(iterator, 2) == nb)
	{
	  return (iterator);
	}
      iterator = iterator + 1;
    }
  return (0);
}
