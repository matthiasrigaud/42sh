/*
** my_getdouble.c for my_getdouble in /home/mag_d/rendu/101pong
** 
** Made by david mag
** Login   <mag_d@epitech.net>
** 
** Started on  Tue Nov  3 12:55:17 2015 david mag
** Last update Mon Nov 30 17:00:13 2015 Matthias RIGAUD
*/

#include <my.h>

int	get_i(char *str, int i)
{
  if (str[i] == '.')
    {
      if (my_str_isnum(&str[i + 1]) != 1)
	my_puterror("Please write correct numbers !\n", 84);
      return (i);
    }
  if (str[i] == 0)
    return (i);
  return (get_i(str, i + 1));
}

double		my_getdouble(char *str)
{
  double	nbr;
  int		i;
  int		size;
  double	ten_pow;
  double	sign;

  sign = 1;
  if (str[0] == 45)
    {
      sign = -1;
      str = &str[1];
    }
  i = get_i(str, 0);
  size = my_strlen(str);
  str[i] = 0;
  if (my_str_isnum(str) != 1)
    my_puterror("Please write correct numbers !\n", 84);
  nbr = my_getnbr(str);
  if (my_strlen(str) != size)
    {
      str = &str[i + 1];
      ten_pow = my_power_rec(10, my_strlen(str));
      nbr += my_getdouble(str) / ten_pow;
    }
  return (sign * nbr);
}
