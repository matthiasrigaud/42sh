/*
** my_str_to_wordtab.c for my_str_to_wordtab in /home/rigaud_b/rendu/j08
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
**
** Started on  Thu Oct  8 08:52:02 2015 Matthias RIGAUD
** Last update Thu Jun  2 21:34:14 2016 Matthias RIGAUD
*/

#include <my.h>
#include <stdlib.h>

int		cond(char c, int can_do_it)
{
  static int	is_quote;

  if (c == '"' && can_do_it)
    is_quote = (is_quote + 1) % 2;
  if (!is_quote && c != 32 && c != 0 && c != '\t' && c != '\v')
    return (1);
  if (is_quote && c != 0)
    return (1);
  return (0);
}

char	**set_wordtab(char **wordtab, char *str)
{
  int	i_word;
  int	i_char;
  int	j;

  i_word = 0;
  i_char = 0;
  j = 0;
  while (str[j] != 0)
    {
      if (cond(str[j], 0))
        {
	  i_char = 0;
          while (cond(str[j], 1))
            {
	      wordtab[i_word][i_char++] = str[j];
              j = j + 1;
            }
	  wordtab[i_word][i_char] = 0;
	  i_word = i_word + 1;
        }
      if (str[j])
	j = j + 1;
    }
  wordtab[i_word] = NULL;
  return (wordtab);
}

int	size_words(char *str, int i)
{
  int	j;
  int	word;
  int	number_words;

  j = 0;
  number_words = 0;
  while (str[j] != 0)
    {
      word = 0;
      if (cond(str[j], 0))
        {
          while (cond(str[j], 1))
            {
              j = j + 1;
	      word = word + 1;
            }
	  if (i == number_words)
	    return (word);
          number_words = number_words + 1;
        }
      if (str[j])
	j = j + 1;
    }
  return (0);
}

int	number_words(char *str)
{
  int	i;
  int	number_words;

  i = 0;
  number_words = 0;
  while (str[i] != 0)
    {
      if (cond(str[i], 0))
	{
	  while (cond(str[i], 1))
	    i++;
	  number_words = number_words + 1;
	}
      if (str[i])
	i = i + 1;
    }
  if (cond(' ', 1))
    {
      cond('"', 1);
      return (-1);
    }
  return (number_words);
}

char	**my_str_to_wordtab(char *str, int *quote)
{
  char	**wordtab;
  int	i;

  if (quote && number_words(str) == -1 && (*quote = 1))
    return (NULL);
  if (quote)
    *quote = 0;
  i = 0;
  wordtab = malloc((number_words(str) + 1) * sizeof(char*));
  if (wordtab == NULL)
    {
      return (NULL);
    }
  while (i != number_words(str))
    {
      wordtab[i] = malloc(size_words(str, i) + 1);
      if (wordtab[i] == NULL)
	{
	  return (NULL);
	}
      i = i + 1;
    }
  return (set_wordtab(wordtab, str));
}
