/*
** my_str_islower.c for my_str_islower in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 14:05:47 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:13:18 2015 matteo melis
*/

#include <my.h>

int	my_str_islower(char *str)
{
  int	i;

  i = 0;
  if (str[0] == 0)
    {
      return (1);
    }
  while (str[i] >= 97 && str[i] <= 122)
    {
      if (str[i + 1] == 0)
	{
	  return (1);
	}
      i = i + 1;
    }
  return (0);
}
