/*
** my_putchar.c for my_putchar in /home/rigaud_b/rendu/my_lib
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Tue Oct  6 11:08:53 2015 Matthias RIGAUD
** Last update Mon Nov  9 16:22:57 2015 Matthias RIGAUD
*/

#include <unistd.h>

int	my_putchar(char c)
{
  write(1, &c, 1);
  return (1);
}
