/*
** my_strncmp.c for my_strncmp in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 11:51:38 2015 Matthias RIGAUD
** Last update Mon Oct  5 13:02:49 2015 Matthias RIGAUD
*/

int	my_strncmp(char *s1, char *s2, int n)
{
  int   i;

  i = 0;
  while (s1[i] != 0 && s2[i] != 0 && i != n)
    {
      if (s1[i] != s2[i])
        {
          return (s1[i] - s2[i]);
        }
      i = i + 1;
    }
  return (0);
}
