/*
** my_getnbr.c for my_getnbr in /home/rigaud_b/rendu/my_lib
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Tue Oct  6 11:25:58 2015 Matthias RIGAUD
** Last update Sun Dec  6 02:08:08 2015 Matthias RIGAUD
*/

#include <my.h>

int	my_getnbr(char *str)
{
  int	i;

  i = 0;
  while (str[i] == 43 || str[i] == 45)
    {
      i = i + 1;
    }
  while (my_char_isnum(str[i]) == 1)
    {
      i = i + 1;
    }
  if (i == 0)
    {
      return (0);
    }
  str[i] = 0;
  return (my_getnbr_base(str, "0123456789"));
}
