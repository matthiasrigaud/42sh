/*
** my_int_to_str.c for my_int_to_str in /home/rigaud_b/rendu/my_lib
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Tue Oct 27 20:17:57 2015 Matthias RIGAUD
** Last update Wed Oct 28 11:13:12 2015 Matthias RIGAUD
*/

#include <stdlib.h>
#include <my.h>

char     *my_stror(int nb, int divide, int signe, char *str)
{
  int   i;

  i = 0;
  if (signe == 1)
    {
      i = 1;
      str[0] = 45;
    }
  while (divide != 0)
    {
      str[i] = (-1 * (nb / divide)) + 48;
      nb = nb - (divide * (nb / divide));
      divide = divide / 10;
      i = i + 1;
    }
  str[i] = 0;
  return (str);
}

char    *my_int_to_str(int nb)
{
  int   nb_clone;
  int   divide;
  int   signe;
  char  *str;

  divide = 1;
  signe = 0;
  if (nb >= 0)
    {
      nb = -1 * nb;
    }
  else
    {
      signe = 1;
    }
  nb_clone = nb;
  while (nb_clone <= -10)
    {
      nb_clone = nb_clone / 10;
      divide = divide * 10;
    }
  str = malloc(divide + 1);
  return (my_stror(nb, divide, signe, str));
}
