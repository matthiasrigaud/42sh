/*
** my_str_isprintable.c for my_str_isprintable in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 16:36:12 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:13:14 2015 matteo melis
*/

#include <my.h>

int	my_str_isprintable(char *str)
{
  int i;

  i = 0;
  if (str[0] == 0)
    {
      return (1);
    }
  while (str[i] >= 32 && str[i] <= 126)
    {
      if (str[i + 1] == 0)
	{
	  return (1);
	}
    }
  return (0);
}
