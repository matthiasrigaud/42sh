/*
** my_strncat.c for my_strncat in /home/rigaud_b/rendu/j07
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Tue Oct  6 14:30:55 2015 Matthias RIGAUD
** Last update Mon Oct 26 09:48:21 2015 matteo melis
*/

#include <my.h>

char	*my_strncat(char *dest, char *src, int nb)
{
  int	i;
  int	j;

  j = 0;
  i = my_strlen(dest);
  while (src[j] != 0 && j != nb)
    {
      dest[i] = src[j];
      j = j + 1;
      i = i + 1;
    }
  dest[i] = 0;
  return (dest);
}
