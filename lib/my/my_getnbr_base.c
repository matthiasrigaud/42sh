/*
** my_getnbr_base.c for my_getnbr_base in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 18:40:23 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:03:36 2015 matteo melis
*/

#include <my.h>

int	test(char str, char *base)
{
  int i;

  i = 0;
  while (base[i] != 0)
    {
      if (base[i] == str || str == 43 || str == 45)
	{
	  return (1);
	}
      i = i + 1;
    }
  return (0);
}

int	m_p(int n, int p)
{
  if (p == 0)
    {
      return (1);
    }
  return (n * m_p(n, p - 1));
}

int	m_n(char *base, char str, int base_size)
{
  int	i;

  i = 0;
  while (i != base_size)
    {
      if (base[i] == str)
	{
	  return (i);
	}
      i = i + 1;
    }
  return (0);
}

int	error_o(char *base, int *base_s, int i)
{
  while (base[*base_s] != 0)
    {
      i = 0;
      while (base[i] != 0)
        {
          if ((base[*base_s] == base[i] && *base_s != i) || base[0] == 0)
            {
              return (0);
            }
          i = i + 1;
        }
      *base_s = *base_s + 1;
    }
  return (1);
}

int	my_getnbr_base(char *str, char *base)
{
  int	nbr;
  int	bas_s;
  int	str_s;
  int	i[2];

  bas_s = 0;
  str_s = 0;
  i[1] = 0;
  nbr = 0;
  i[0] = 0;
  if (error_o(base, &bas_s, 0) == 0 || error_t(str, base, &str_s, i) == 0)
    {
      return (0);
    }
  while (i[0] != str_s)
    {
      nbr = nbr - (m_p(bas_s, str_s - 1 - i[0]) * m_n(base, str[i[0]], bas_s));
      i[0] = i[0] + 1;
    }
  if (i[1] % 2 == 0)
    {
      nbr = -nbr;
    }
  return (nbr);
}
