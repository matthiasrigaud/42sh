/*
** my_str_isnum.c for my_str_num in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 14:05:47 2015 Matthias RIGAUD
** Last update Thu Oct  8 10:56:50 2015 Matthias RIGAUD
*/

int	my_str_isnum(char *str)
{
  int	i;

  i = 0;
  if (str[0] == 0)
    {
      return (1);
    }
  while (str[i] >= 48 && str[i] <= 57)
    {
      if (str[i + 1] == 0)
	{
	  return (1);
	}
      i = i + 1;
    }
  return (0);
}
