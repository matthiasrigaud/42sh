/*
** my_putnbr_base.c for my_putnbr_base in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 16:52:13 2015 Matthias RIGAUD
** Last update Thu Nov 12 11:02:22 2015 Matthias RIGAUD
*/

#include <my.h>

long int	searching_power_uns(long unsigned int num,
				    long unsigned int nbr,
				    long int *power,
				    long unsigned int *num_pow)
{
  *num_pow = 1;
  *power = 0;
  while (*num_pow <= nbr)
    {
      *num_pow = *num_pow * num;
      *power = *power + 1;
    }
  *power = *power - 1;
  *num_pow = *num_pow / num;
  return (*power);
}

int	searching_number_uns(long unsigned int numbase_powered,
			     long unsigned int *number,
			     long unsigned int nbr)
{
  *number = 1;
  if (nbr != 0)
    {
      while (*number * numbase_powered <= nbr)
	{
	  *number = *number + 1;
	}
      *number = *number - 1;
    }
  return (0);
}

long unsigned int	initialise_uns(long unsigned int *numbase,
				       char *base,
				       long unsigned int *nb,
				       long unsigned int nbr)
{
  *numbase = 0;
  while (base[*numbase] != 0)
    {
      *numbase = *numbase + 1;
    }
  *nb = nbr;
  if (nbr == 0)
    {
      my_putchar(48);
    }
  nbr = 0;
  if (*nb > 999999999)
    {
      nbr = *nb % *numbase;
      *nb = *nb / *numbase;
    }
  return (nbr);
}

int	end_uns(long int *i, long unsigned int *nb_plus,
		long unsigned int *nb, char *base)
{
  int	r;

  r = 0;
  if (i[1] != i[0])
  {
    r += my_putchar(base[0]);
  }
  i[0] = i[0] - 1;
  if (i[0] == -1 && *nb_plus != 0)
    {
      *nb = *nb_plus;
      i[0] = 0;
      *nb_plus = 0;
    }
  return (r);
}

int			my_putnbr_base_uns(long unsigned int nbr,
					   char *base)
{
  long unsigned int	numbase;
  long unsigned int	numbase_powered;
  long unsigned int	number;
  long int		i[2];
  long unsigned int	nb;
  long unsigned int	nb_plus;
  int			r;

  nb_plus = initialise_uns(&numbase, base, &nb, nbr);
  i[0] = searching_power_uns(numbase, nb, &i[1], &numbase_powered);
  r = 0;
  if (nbr == 0)
    r += 1;
  while (i[0] != -1)
    {
      searching_power_uns(numbase, nb, &i[1], &numbase_powered);
      searching_number_uns(numbase_powered, &number, nb);
      if (i[1] == i[0])
	{
	  r += my_putchar(base[number]);
	  nb = nb - (number * numbase_powered);
	}
      r += end_uns(i, &nb_plus, &nb, base);
    }
  return (r);
}
