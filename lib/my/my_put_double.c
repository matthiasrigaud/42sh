/*
** my_put_double.c for 101pong in /home/rigaud_b/rendu/101pong
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Wed Nov  4 16:20:52 2015 Matthias RIGAUD
** Last update Wed Nov 25 12:27:09 2015 Matthias RIGAUD
*/

#include <my.h>

int	my_put_double(double	nbr)
{
  int	sup_int;
  int	inf_int;
  int	r;

  if (nbr < 0)
    {
      r = my_putchar(45);
      nbr = -nbr;
    }
  r = my_put_nbr(nbr);
  inf_int = nbr;
  nbr = nbr -inf_int;
  inf_int = 0;
  sup_int = 1;
  if (nbr > 0)
    r += my_putchar('.');
  while (inf_int < nbr && sup_int > nbr)
    {
      nbr = nbr -inf_int;
      nbr *= 10;
      inf_int = nbr;
      sup_int = nbr + 1;
      r += my_put_nbr(nbr);
    }
  return (r);
}
