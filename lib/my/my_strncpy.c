/*
** my_strncpy.c for my_strncpy in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 09:48:48 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:13:07 2015 matteo melis
*/

#include <my.h>

char	*my_strncpy(char *dest, char *src, int n)
{
  int	i;

  i = 0;
  while (i != n)
    {
      dest[i] = src[i];
      if (src[i] == 0)
	{
	  i = n - 1;
	}
      i = i + 1;
    }
  return (dest);
}
