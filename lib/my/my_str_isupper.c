/*
** my_str_isupper.c for my_str_isupper in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 14:05:47 2015 Matthias RIGAUD
** Last update Tue Oct  6 16:46:09 2015 Matthias RIGAUD
*/

int	my_str_isupper(char *str)
{
  int	i;

  i = 0;
  if (str[0] == 0)
    {
      return (1);
    }
  while (str[i] >= 65 && str[i] <= 90)
    {
      if (str[i + 1] == 0)
	{
	  return (1);
	}
      i = i + 1;
    }
  return (0);
}
