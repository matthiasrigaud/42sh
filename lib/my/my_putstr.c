/*
** my_pustr.c for my_pustr in /home/rigaud_b/rendu/j04
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Thu Oct  1 09:20:12 2015 Matthias RIGAUD
** Last update Mon Nov  9 16:39:24 2015 Matthias RIGAUD
*/

#include <unistd.h>
#include <my.h>

int	my_putstr(char *str)
{
  write(1, str, my_strlen(str));
  return (my_strlen(str));
}
