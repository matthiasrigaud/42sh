/*
** my_str_isanumber.c for libmy in /home/rigaud_b/rendu/103architect/lib/my
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Nov 30 15:01:17 2015 Matthias RIGAUD
** Last update Mon Nov 30 15:08:53 2015 Matthias RIGAUD
*/

#include <my.h>

int	my_str_isanumber(char *str)
{
  int	i;

  i = 0;
  if (str[0] == 0)
    {
      return (1);
    }
  while (my_char_isanumber(str[i]))
    {
      if (str[i + 1] == 0)
	{
	  return (1);
	}
      i = i + 1;
    }
  return (0);
}
