/*
** my_find_prime_sup.c for my_find_prime_sup in /home/rigaud_b/rendu/j05
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Fri Oct  2 23:20:40 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:07:42 2015 matteo melis
*/

#include <my.h>

int	my_find_prime_sup(int nb)
{
  if (my_is_prime(nb) == 1)
    {
      return (nb);
    }
  else
    {
      while (my_is_prime(nb) != 1)
	{
	  nb = nb + 1;
	}
      return (nb);
    }
}
