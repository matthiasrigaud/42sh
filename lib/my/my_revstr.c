/*
** my_revstr.c for my_revstr in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 10:29:26 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:07:59 2015 matteo melis
*/

#include <my.h>

char	*my_revstr(char *str)
{
  int	size_str;
  int	i;
  char	c;

  i = 0;
  size_str = my_strlen(str);
  while (i != size_str / 2)
    {
      c = str[i];
      str[i] = str[size_str -(1 + i)];
      str[size_str -(i + 1)] = c;
      i = i + 1;
    }
  return (str);
}
