/*
** my_sort_int_tab.c for my_sort_int_tab in /home/rigaud_b/rendu/my_lib
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Tue Oct  6 17:36:52 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:13:31 2015 matteo melis
*/

#include <my.h>

int	my_test_i(int *tab_i, int i, int size)
{
  int	j;

  j = 0;
  while (j != size)
    {
      if (tab_i[j] == i)
	{
	  return (0);
	}
      j = j + 1;
    }
  return (1);
}

int	*my_equalizor(int *tab, int *tab_t, int size)
{
  int	i;

  i = 0;
  while (i != size)
    {
      tab[i] = tab_t[i];
      i = i + 1;
    }
  return (0);
}

int	*inicializor_i(int *j, int *max_test)
{
  *j = 0;
  *max_test = 0;
  return (0);
}

int	*my_incrementor_i(int *j, int *max_test)
{
  *max_test = 0;
  *j = *j + 1;
  return (0);
}

void	my_sort_int_tab(int *tab, int size)
{
  int	i;
  int	j;
  int	tab_t[size];
  int	tab_i[size];
  int	max;
  int	max_test;

  inicializor_i(&j, &max_test);
  while (j != size)
    {
      i = 0;
      while (i != size)
	{
	  if ((max_test == 0 || tab[i] >= max) && my_test_i(tab_i, i, j) == 1)
	    {
	      max = tab[i];
	      tab_i[j] = i;
	      max_test = 1;
	    }
	  i = i + 1;
	}
      tab_t[size - j - 1] = max;
      my_incrementor_i(&j, &max_test);
    }
  my_equalizor(tab, tab_t, size);
}
