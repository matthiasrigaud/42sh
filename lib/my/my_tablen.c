/*
** my_tablen.c for tablen in /home/melis_m/rendu/Piscine_C_evalexpr/lib/my
** 
** Made by matteo melis
** Login   <melis_m@epitech.net>
** 
** Started on  Thu Oct 22 10:07:45 2015 matteo melis
** Last update Mon Oct 26 10:12:49 2015 matteo melis
*/

#include <my.h>
#include <stdlib.h>

int	my_tablen(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != NULL)
    {
      i = i + 1;
    }
  return (i);
}
