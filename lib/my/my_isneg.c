/*
** my_isneg.c for my_isneg in /home/rigaud_b/rendu/j03
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Wed Sep 30 11:11:02 2015 Matthias RIGAUD
** Last update Mon Oct 26 11:27:22 2015 Matthias RIGAUD
*/

#include <my.h>

int	my_isneg(int nb)
{
  if (nb < 0)
    {
      my_putchar('N');
      return (1);
    }
  my_putchar('P');
  return (0);
}
