/*
** my_strlowcase.c for my_strlowcase in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 13:21:26 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:20:36 2015 matteo melis
*/

#include <my.h>

char	*my_strlowcase(char *str)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    {
      if (str[i] >= 65 && str[i] <= 90)
	{
	  str[i] = str[i] + 32;
	}
      i = i + 1;
    }
  return (str);
}
