/*
** my_putnbr_base.c for my_putnbr_base in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 16:52:13 2015 Matthias RIGAUD
** Last update Tue Nov 10 10:07:26 2015 Matthias RIGAUD
*/

#include <my.h>

int	searching_power(unsigned int num, unsigned int nbr,
			int *power, unsigned int *num_pow)
{
  *num_pow = 1;
  *power = 0;
  while (*num_pow <= nbr)
    {
      *num_pow = *num_pow * num;
      *power = *power + 1;
    }
  *power = *power - 1;
  *num_pow = *num_pow / num;
  return (*power);
}

int	searching_number(unsigned int numbase_powered,
			 unsigned int *number, unsigned int nbr)
{
  *number = 1;
  if (nbr != 0)
    {
      while (*number * numbase_powered <= nbr)
	{
	  *number = *number + 1;
	}
      *number = *number - 1;
    }
  return (0);
}

int	initialise(unsigned int *numbase, char *base,
		   unsigned int *nb, int nbr)
{
  *numbase = 0;
  while (base[*numbase] != 0)
    {
      *numbase = *numbase + 1;
    }
  if (nbr < 0)
    {
      my_putchar(45);
      *nb = -nbr;
    }
  else if (nbr > 0)
    {
      *nb = nbr;
    }
  else
    {
      my_putchar(48);
    }
  nbr = 0;
  if (*nb > 999999999)
    {
      nbr = *nb % *numbase;
      *nb = *nb / *numbase;
    }
  return (nbr);
}

int	end(int *i, unsigned int *nb_plus,
	    unsigned int *nb, char *base)
{
  int	r;

  r = 0;
  if (i[1] != i[0])
  {
    r += my_putchar(base[0]);
  }
  i[0] = i[0] - 1;
  if (i[0] == -1 && *nb_plus != 0)
    {
      *nb = *nb_plus;
      i[0] = 0;
      *nb_plus = 0;
    }
  return (r);
}

int		my_putnbr_base(int nbr, char *base)
{
  unsigned int	numbase;
  unsigned int	numbase_powered;
  unsigned int	number;
  int		i[2];
  unsigned int	nb;
  unsigned int	nb_plus;
  int		r;

  nb_plus = initialise(&numbase, base, &nb, nbr);
  i[0] = searching_power(numbase, nb, &i[1], &numbase_powered);
  r = 0;
  if (nbr <= 0)
    r += 1;
  while (i[0] != -1)
    {
      searching_power(numbase, nb, &i[1], &numbase_powered);
      searching_number(numbase_powered, &number, nb);
      if (i[1] == i[0])
	{
	  r += my_putchar(base[number]);
	  nb = nb - (number * numbase_powered);
	}
      r += end(i, &nb_plus, &nb, base);
    }
  return (r);
}
