/*
** my_char_isanumber.c for libmy in /home/rigaud_b/rendu/103architect/lib/my
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Nov 30 15:04:16 2015 Matthias RIGAUD
** Last update Mon Nov 30 15:08:37 2015 Matthias RIGAUD
*/

#include <my.h>

int	my_char_isanumber(char c)
{
  if (my_char_isnum(c) ||
      c == 45 ||
      c == 46)
    {
      return (1);
    }
  else
    {
      return (0);
    }
}
