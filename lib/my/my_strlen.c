/*
** my_strlen.c for my_strlen in /home/rigaud_b/rendu/j04
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Thu Oct  1 11:18:05 2015 Matthias RIGAUD
** Last update Mon Oct 26 14:22:07 2015 Matthias RIGAUD
*/

#include <my.h>

int	my_strlen(char *str)
{
  int	str_size;

  str_size = 0;
  while	(str[str_size] != '\0')
    {
      str_size = str_size + 1;
    }
  return (str_size);
}
