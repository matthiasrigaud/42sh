/*
** my_strcapitalize.c for my_strcapitalize in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Mon Oct  5 13:34:01 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:13:26 2015 matteo melis
*/

#include <my.h>

char *my_strcapitalize(char *str)
{
  int	i;
  int	a;
  int	p;

  i = 0;
  while (str[i] != 0)
    {
      if (str[i] >= 65 || str[i] <= 90)
	{
	  str[i] = str[i] + 32;
	}
      a = str[i];
      p = str[i - 1];
      if (a >= 97 && a <= 122 && (i == 0 || p == 32 || p == 43 || p == 45))
	{
	  str[i] = str[i] - 32;
	}
      i = i + 1;
    }
  return (str);
}
