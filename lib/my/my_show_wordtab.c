/*
** my_show_wordtab.c for my_show_wordtab in /home/rigaud_b/rendu/j08
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
**
** Started on  Wed Oct  7 14:37:36 2015 Matthias RIGAUD
** Last update Sun Jun  5 00:24:18 2016 Matthias RIGAUD
*/

#include <my.h>

int	my_show_wordtab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != 0)
    {
      my_putstr(tab[i]);
      if (tab[i + 1])
	my_putchar(32);
      i = i + 1;
    }
  my_putchar(10);
  return (0);
}
