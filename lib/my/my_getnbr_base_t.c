/*
** my_getnbr_base_t.c for my_getnbr_base in /home/rigaud_b/rendu/j06
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Tue Oct  6 09:22:36 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:01:27 2015 matteo melis
*/

#include <my.h>

int	error_t(char *str, char *base, int *str_s, int *minus)
{
  int	i;

  i = 0;
  while (str[i] != 0)
    {
      if (test(str[i], base) == 0 || str[0] == 0)
        {
          return (0);
        }
      if (str[i] == 45)
        {
          minus[1] = minus[1] + 1;
	  minus[0] = minus[0] + 1;
        }
      else if (str[i] == 43)
        {
	  minus[0] = minus[0] + 1;
        }
      i = i + 1;
    }
  *str_s = i;
  return (1);
}
