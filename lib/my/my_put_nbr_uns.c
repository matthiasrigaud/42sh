/*
** my_put_nbr.c for my_put_nbr in /home/rigaud_b/rendu/j03
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Wed Sep 30 14:23:47 2015 Matthias RIGAUD
** Last update Mon Nov  9 16:51:33 2015 Matthias RIGAUD
*/

#include <my.h>

int	my_displayor_three_uns(unsigned int nb, unsigned int divide, int r)
{
  while (divide != 0)
    {
      r += my_putchar((nb / divide) + 48);
      nb = nb -(divide * (nb /divide));
      divide = divide / 10;
    }
  return (r);
}

int		my_put_nbr_uns(unsigned int nb)
{
  unsigned int	nb_clone;
  unsigned int	divide;
  int	r;

  divide = 1;
  r = 0;
  nb_clone = nb;
  while (nb_clone >= 10)
    {
      nb_clone = nb_clone / 10;
      divide = divide * 10;
    }
  return (my_displayor_three_uns(nb, divide, r));
}
