/*
** my_strcat.c for my_strcat in /home/rigaud_b/rendu/j07
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Tue Oct  6 14:30:55 2015 Matthias RIGAUD
** Last update Mon Oct 26 17:33:51 2015 Matthias RIGAUD
*/

#include <my.h>

char	*my_strcat(char *dest, char *src)
{
  int	j;
  int	i;

  j = 0;
  i = my_strlen(dest);
  while (src[j] != 0)
    {
      dest[i] = src[j];
      j = j + 1;
      i = i + 1;
    }
  dest[i] = 0;
  return (dest);
}
