/*
** my_swap.c for my_swap in /home/rigaud_b/rendu/j04
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Thu Oct  1 08:50:55 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:12:57 2015 matteo melis
*/

#include <my.h>

int	my_swap(int *a, int *b)
{
  int	c;

  c = *a;
  *a = *b;
  *b = c;
  return (0);
}
