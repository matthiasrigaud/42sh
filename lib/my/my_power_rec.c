/*
** my_power_rec.c for my_power_rec in /home/rigaud_b/rendu/j05
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Fri Oct  2 18:01:58 2015 Matthias RIGAUD
** Last update Mon Oct 26 10:13:42 2015 matteo melis
*/

#include <my.h>

int	my_power_rec(int nb, int power)
{
  if (power == 0)
    {
      return (1);
    }
  else if (power < 0)
    {
      return (0);
    }
  else
    {
      return (nb * my_power_rec(nb, power - 1));
    }
}
