/*
** my_char_isalpha.c for libmy in /home/rigaud_b/rendu/PSU_2015_my_exec/lib/my
** 
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
** 
** Started on  Thu Jan  7 11:26:41 2016 Matthias RIGAUD
** Last update Thu Jan  7 11:27:51 2016 Matthias RIGAUD
*/

int	my_char_isalpha(char c)
{
  if ((c >= 65 && c <= 90) || (c >= 97 && c <= 122))
    return (1);
  return (0);
}
