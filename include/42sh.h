/*
** 42sh.h for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/include
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:39:21 2016 Matthias RIGAUD
** Last update Sun Jun  5 17:25:11 2016 Matthias RIGAUD
*/

#ifndef SH_H_
# define SH_H_

/*
** Include
*/

# include <my.h>
# include <unistd.h>
# include <stdlib.h>
# include <sys/wait.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <get_next_line.h>
# include <signal.h>
# include <fcntl.h>
# include <stdio.h>
# include <errno.h>
# include <string.h>

/*
** define
*/

# define BORN(a, x, b)	(x < a ? (a) : (x > b ? (b) : x))

# define DEFAULT_PATH	"PATH=/usr/bin:/bin:/usr/sbin:/sbin"

# define PROMPT_VAR	"PS1"

# define DEFAULT_PROMPT	"$> "

# define DEFAULT_FILE	".42shrc"

# define CNF		"Command not found"

# define EXEC_ERR	"Exec format error. Binary file not executable"

# define CMD_ERROR(x)	(x == 2 ? CNF : x == 8 ? EXEC_ERR : strerror(x))

# define B		inf.back

# define ISB(x)		(x == 4 ? 1 : 0)

# define CHG_RET	must_exit("exit", NULL, 0) >= -42

/*
** Struct && typedef
*/

typedef struct	stat	t_stt;

typedef struct	s_io
{
  int		in;
  int		out;
}		t_io;

typedef struct	s_als
{
  char		*old;
  char		*alias;
  struct s_als	*next;
}		t_als;

typedef struct	s_inf
{
  t_io		fd;
  int		back;
}		t_inf;

/*
** Functions
*/

int	tabfree(char ***prog);
int	my_exec(char *name, char **prog, char **env);
char	*find_commande(char *prog, char **env);
void	*xmalloc(int size);
char	**tmalloc(int size);
int	cd(char **com, char *home, char ***env);
int	find_line(char **env, char *field);
void	ctrl_c(int sig);
int	is_ctrl_c(int sig);
int	do_env(char **env);
void	tab_cat(char ***tab, char **str);
int	setenv(char ***env, char **com);
int	unsetenv(char ***env, char **com);
int	is_redirect_out(char ***argv, int *fd);
int	set_redirect_out(int new_fd, int old_fd);
int	is_redirect_in(char ***argv, int *fd);
int	set_redirect_in(int new_fd, int old_fd);
void	my_tab_comp(int position, int nb, char ***av);
int	must_exit(char *brut, int *set, int is_ctrl_c);
void	prep_str(char **str, t_als *alias);
char	**tab_dup(char **tab);
void	step_by_step_exec(char ***env, char ***prog, int size, t_als **alias);
int	send_exec(char ***env, char ***prog, t_inf *inf, t_als **alias);
void	my_close(int fd);
int	replace_dollar(char **env, char ***prog, int end);
void	disp_prompt(char **env);
void	final_free(void);
int	char_is_in_str(char *str, char c);
int	delete_quote(char ***prog);
void	rc_reader(char ***env);
void	prep_exec(char ***env, char **prog_brut, char ***prog, int i_cc_p);
int	make_alias(char ***prog, t_als **alias);
void	replace_alias(char **str, t_als *alias, int level);
int	do_echo(char ***prog);
int	is_spe_car(char c, int can_do_that);
void	end_back(int end, char **prog);

/*
** Pointeurs sur Fonctions
*/

void	create_builtins(void (*built[8])(char ***, char ***, int *, t_als **));
void	built_cd(char ***env, char ***prog, int *end, t_als **alias);
void	built_env(char ***env, char ***prog, int *end, t_als **alias);
void	built_setenv(char ***env, char ***prog, int *end, t_als **alias);
void	built_unsetenv(char ***env, char ***prog, int *end, t_als **alias);
void	built_echo(char ***env, char ***prog, int *end, t_als **alias);
void	built_alias(char ***env, char ***prog, int *end, t_als **alias);
void	built_exec(char ***env, char ***prog, int *end, t_als **alias);
int	which_builtin(char *cmd);

/*
** list
*/

void	free_elem(t_als *elem);
void	delete_elem(t_als **alias, char *str);
void	add_elem(t_als **alias, char *old, char *new);
void	disp_list(t_als *alias);

/*
** End
*/

#endif /* !SH_H_ */
