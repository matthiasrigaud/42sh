/*
** my.h for  in /home/rigaud_b/rendu/j09
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
**
** Started on  Thu Oct  8 09:46:07 2015 Matthias RIGAUD
** Last update Thu Jun  2 21:32:55 2016 Matthias RIGAUD
*/

#ifndef MY_H_
# define MY_H_

int	my_putchar(char c);
int	my_isneg(int nb);
int	my_put_nbr(int nb);
int	my_swap(int *a, int *b);
int	my_putstr(char *str);
int	my_strlen(char *str);
int	my_getnbr(char *str);
void	my_sort_int_tab(int *tab, int size);
int	my_power_rec(int nb, int power);
int	my_square_root(int nb);
int	my_is_prime(int nombre);
int	my_find_prime_sup(int nb);
char	*my_strcpy(char *dest, char *src);
char	*my_strncpy(char *dest, char *src, int nb);
char	*my_revstr(char *str);
char	*my_strstr(char *str, char *to_find);
int	my_strcmp(char *s1, char *s2);
int	my_strncmp(char *s1, char *s2, int nb);
char	*my_strupcase(char *str);
char	*my_strlowcase(char *str);
char	*my_strcapitalize(char *str);
int	my_str_isalpha(char *str);
int	my_str_isnum(char *str);
int	my_str_isanumber(char *str);
int	my_str_islower(char *str);
int	my_str_isupper(char *str);
int	my_str_isprintable(char *str);
int	my_showstr(char *str);
int	my_showmem(char *str, int size);
char	*my_strcat(char *dest, char *src);
char	*my_strncat(char *dest, char *src, int nb);
int	my_strlcat(char *dest, char *src, int size);
int	my_putnbr_base(int nbr, char *base);
int	my_getnbr_base(char *str, char*base);
char	**my_str_to_wortab(char *str);
int	my_char_isnum(char c);
int	my_char_isanumber(char c);
int	my_char_isalpha(char c);
int	my_show_wordtab(char **tab);
char    *my_strdup(char *src);
char    **my_str_to_wordtab(char *str, int *quote);
char	*operations(char **tab);
int	my_tablen(char **tab);
char	**my_tabcat(char **tab1, char **tab2);
int	error_t(char *str, char *base, int *str_s, int *minus);
int	test(char str, char *base);
char	*my_int_to_str(int nb);
char	*my_stror(int nb, int divide, int signe, char *str);
int     my_putnbr_base_uns(long unsigned int nbr, char *base);
int     my_printf(const char *format, ...);
int     my_put_nbr_uns(unsigned int nb);
double	my_getdouble(char *str);
int	get_i(char *str, int i);
int	my_put_double(double nbr);
int	my_puterror(char *cause, int exit);

#endif /* !MY_H_ */
