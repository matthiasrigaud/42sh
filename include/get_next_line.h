/*
** get_next_line.h for get_next_line in /home/rigaud_b/rendu/CPE_2015_getnextline
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.net>
**
** Started on  Thu Jan  7 23:18:09 2016 Matthias RIGAUD
** Last update Wed Jun  1 16:18:05 2016 clement raposo
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

/*
** Include
*/

#include <unistd.h>
#include <stdlib.h>

/*
** Define
*/

#ifndef READ_SIZE
# define READ_SIZE (2048)
#endif /* READ_SIZE */

/*
** Struct && typedef
*/

typedef struct	s_buf
{
  char		*str;
  int		size;
}		t_buf;

/*
** Functions
*/

char	*get_next_line(const int fd, int erase);
int	is_10_in(t_buf *buf);
int	purge_buffer(t_buf *buf);
int	free_buffer(int erase, t_buf *buffer);
int	add_n_car(t_buf *dest, t_buf *src, int n);
int	reader(t_buf *buffer, t_buf *line, const int fd);

/*
** End
*/

#endif /* !GET_NEXT_LINE_H_ */
