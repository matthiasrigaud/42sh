/*
** tab_utilities.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:44:48 2016 Matthias RIGAUD
** Last update Sun Jun  5 17:26:10 2016 Matthias RIGAUD
*/

# include <42sh.h>

char	**tmalloc(int size)
{
  char	**returner;

  if (!(returner = malloc((size + 1) * sizeof(char *))))
    my_puterror("Error malloc\n", 42);
  return (returner);
}

int	tabfree(char ***prog)
{
  int   i;

  i = -1;
  while ((*prog)[++i])
    free((*prog)[i]);
  free(*prog);
  return (1);
}

void	my_tab_comp(int position, int nb, char ***av)
{
  char	**new;
  int	i;

  new = tmalloc(my_tablen(*av) - nb);
  i = -1;
  while ((*av)[++i])
    if (i < position && !(new[i] = my_strdup((*av)[i])))
      my_puterror("Error malloc\n", 42);
    else if (i >= position + nb && !(new[i - nb] = my_strdup((*av)[i])))
      my_puterror("Error malloc\n", 42);
  tabfree(av);
  new[i - nb] = NULL;
  *av = new;
}

char	**tab_dup(char **tab)
{
  char	**new;
  int	i;

  new = tmalloc(my_tablen(tab));
  i = -1;
  while (tab[++i])
    if (!(new[i] = my_strdup(tab[i])))
      my_puterror("Error malloc\n", 42);
  new[i] = NULL;
  return (new);
}
