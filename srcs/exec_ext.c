/*
** exec_ext.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:42:27 2016 Matthias RIGAUD
** Last update Sun Jun  5 17:31:26 2016 Matthias RIGAUD
*/

# include <42sh.h>

void	make_separator(int *separator, int value, char **str)
{
  *separator = value;
  free(*str);
  *str = NULL;
}

void	prep_tab(char ***prog, int *separator)
{
  int	i;
  int	j;

  i = -1;
  j = -1;
  while ((*prog)[++i])
    if (!my_strcmp((*prog)[i], ";"))
      make_separator(&separator[++j], 0, &(*prog)[i]);
    else if (!my_strcmp((*prog)[i], "&&"))
      make_separator(&separator[++j], 2, &(*prog)[i]);
    else if (!my_strcmp((*prog)[i], "||"))
      make_separator(&separator[++j], 3, &(*prog)[i]);
    else if (!my_strcmp((*prog)[i], "|"))
      make_separator(&separator[++j], 1, &(*prog)[i]);
    else if (!my_strcmp((*prog)[i], "&"))
      make_separator(&separator[++j], 4, &(*prog)[i]);
  separator[++j] = 0;
}

void		step_by_step_exec(char	***env,
				  char	***prog,
				  int	size,
				  t_als	**alias)
{
  char		**tmp;
  int		i;
  int		j;
  int		pipe_fd[2];
  int		sep[1000];
  t_inf		inf;
  static int	end;

  if ((i = j = inf.fd.in = -1) && ((!env && !prog && !size && (end = 1))
				   || (end = replace_dollar(*env, prog, end))))
    return ;
  prep_tab(prog, sep);
  while (++i < size && (pipe_fd[0] = -1) && (pipe_fd[1] = -1))
    {
      if ((i += my_tablen((tmp = tab_dup(&(*prog)[i])))) && sep[++j] == 1)
	pipe(pipe_fd);
      if ((inf.fd.out = pipe_fd[1]) && delete_quote(&tmp) && (B = ISB(sep[j]))
	  >= 0 && (((end = send_exec(env, &tmp, &inf, alias)) && sep[j] == 2) ||
	   (!end && sep[j] == 3 && CHG_RET)) && tabfree(&tmp))
	return ;
      if (j && !sep[j - 1] && !end)
	must_exit("exit", NULL, 0);
      if ((inf.fd.in = pipe_fd[0]) && i < size)
	(*prog)[i] = my_strdup("");
    }
}
