/*
** main.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:43:22 2016 Matthias RIGAUD
** Last update Sun Jun  5 17:43:06 2016 Matthias RIGAUD
*/

# include <42sh.h>

int	my_exec(char *name, char **prog, char **env)
{
  int	statu;
  int	pid;

  prog[0] = name;
  signal(SIGINT, SIG_IGN);
  if (!(pid = fork()))
    {
      signal(SIGINT, SIG_DFL);
      if (execve(prog[0], prog, env) < 0)
	{
	  fprintf(stderr, "%s: %s.\n", prog[0], CMD_ERROR(errno));
	  exit(1);
	}
    }
  else if (pid == -1)
    my_puterror("I can't create process.\n", 1);
  else
    wait(&statu);
  must_exit(NULL, &statu, 0);
  if (statu == SIGSEGV || statu == 139)
    fprintf(stderr, "Segmentation fault (core dumped)\n");
  if (statu == 136 || statu == SIGFPE)
    fprintf(stderr, "Floating exception (core dumped)\n");
  signal(SIGINT, ctrl_c);
  return (statu / 256);
}

int		send_exec(char ***env, char ***prog, t_inf *inf, t_als **alias)
{
  int		end;
  static void	(*blt[8])(char ***, char ***, int *, t_als **);
  int		pid;

  if ((end = 256) && blt[0] == NULL)
    create_builtins(blt);
  if (inf->back && (pid = fork()) == -1)
    my_puterror("I can't create process.\n", 1);
  if (inf->back && pid && my_printf("[x] %i\n", pid))
    return (0);
  if (((is_redirect_in(prog, &inf->fd.in)) == -2 ||
      set_redirect_in(0, inf->fd.in)) && must_exit(NULL, &end, 1))
    end = 1 + my_puterror("Missing name for redirect.\n", 0);
  else if (((is_redirect_out(prog, &inf->fd.out)) == -2
	   || set_redirect_out(1, inf->fd.out)) && must_exit(NULL, &end, 1))
    end = 1 + my_puterror("Missing name for redirect.\n", 0);
  else
    blt[which_builtin((*prog)[0])](env, prog, &end, alias);
  if (inf->back)
    end_back(end, *prog);
  my_close(inf->fd.in);
  my_close(inf->fd.out);
  set_redirect_out(1, 1);
  set_redirect_in(0, 0);
  return (end);
}

void		prep_exec(char	***env,
			  char	**prog_brut,
			  char	***prog,
			  int	i_cc_p)
{
  char		*home;
  int		quote;
  static t_als	*alias;

  if (!i_cc_p)
    {
      prep_str(prog_brut, alias);
      if (!(*prog = my_str_to_wordtab(*prog_brut, &quote)) && quote)
	{
	  fprintf(stderr, "Unmatched \".\n");
	  step_by_step_exec(NULL, NULL, 0, NULL);
	  return ;
	}
      if (!(*prog))
	my_puterror("Error Malloc\n", 42);
      if (find_line(*env, "HOME") == -1 && !my_strcmp("cd", (*prog)[0]))
	{
	  if (!(home = my_strdup("HOME=/home")))
	    my_puterror("Error memory", 1);
	  tab_cat(env, &home);
	}
      step_by_step_exec(env, prog, my_tablen(*prog), &alias);
      tabfree(prog);
    }
  free(*prog_brut);
}

char	**get_env()
{
  char	**env;
  char	*pwd;

  pwd = xmalloc(100);
  pwd = my_strcpy(pwd, "PWD=");
  getcwd(&pwd[4], 95);
  if (!(env = malloc(4 * sizeof(char *))))
    my_puterror("Error memory\n", 1);
  if (!(env[0] = my_strdup("HOME=/home")))
    my_puterror("Error memory", 1);
  env[1] = pwd;
  if (!(env[2] = my_strdup("PATH=/bin:/sbin:/usr/bin:/usr/sbin:"
			   "/usr/heimdal/bin:/usr/heimdal/sbin")))
    my_puterror("Error memory", 1);
  env[3] = NULL;
  return (env);
}

int	main(int ac, char **av, char **env)
{
  char	*prog_brut;
  char	**prog;
  int	i_cc_p;
  int	ex_nb;

  (void)ac;
  (void)av;
  signal(SIGINT, ctrl_c);
  if (!env[0])
    env = get_env();
  rc_reader(&env);
  disp_prompt(env);
  prog_brut = get_next_line(0, 0);
  i_cc_p = is_ctrl_c(0);
  while ((ex_nb = must_exit(prog_brut, NULL, i_cc_p)) < 0
	 && (prog_brut || (!prog_brut && i_cc_p)))
    {
      signal(SIGINT, ctrl_c);
      prep_exec(&env, &prog_brut, &prog, i_cc_p);
      disp_prompt(env);
      prog_brut = get_next_line(0, i_cc_p);
      i_cc_p = is_ctrl_c(0);
    }
  final_free();
  return (ex_nb);
}
