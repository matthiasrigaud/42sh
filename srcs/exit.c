/*
** exit.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:42:52 2016 Matthias RIGAUD
** Last update Sun Jun  5 17:48:08 2016 Matthias RIGAUD
*/

# include <42sh.h>

int		must_exit(char *brut, int *set, int is_ctrl_c)
{
  int		ex_nb;
  char		**com;
  int		quote;
  static int	ctrl_d_ret;

  if (set)
    ctrl_d_ret = (*set == 139 || *set == SIGSEGV ? 139 : *set == 136 ||
		  *set == SIGFPE ? 136 : *set ? *set / 256 : ctrl_d_ret);
  if (brut)
    ctrl_d_ret = 0;
  if (brut == NULL)
    return ((is_ctrl_c ? -1 : ctrl_d_ret));
  if (!(com = my_str_to_wordtab(brut, &quote)) && !quote)
    my_puterror("Error Malloc\n", 42);
  if (quote)
    return (-1);
  if (my_tablen(com) == 1 && !my_strcmp(com[0], "exit"))
    ex_nb = 0;
  else if (my_tablen(com) == 2 && !my_strcmp(com[0], "exit")
      && my_str_isnum(com[1]))
    ex_nb = my_getnbr(com[1]);
  else
    ex_nb = -1;
  tabfree(&com);
  return (ex_nb);
}
