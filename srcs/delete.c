/*
** delete.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Fri Jun  3 11:30:30 2016 Matthias RIGAUD
** Last update Sun Jun  5 17:27:11 2016 Matthias RIGAUD
*/

# include <42sh.h>

int	delete_quote(char ***prog)
{
  int	i;
  int	j;
  int	k;

  i = -1;
  while ((*prog)[++i] && (j = -1))
    while ((*prog)[i][++j])
      if ((*prog)[i][j] == '"' && (k = j - 1) >= -1)
	while ((*prog)[i][++k])
	  (*prog)[i][k] = (*prog)[i][k + 1];
  return (1);
}
