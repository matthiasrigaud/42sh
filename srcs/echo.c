/*
** echo.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Sat Jun  4 02:34:46 2016 Matthias RIGAUD
** Last update Sat Jun  4 18:26:09 2016 Matthias RIGAUD
*/

# include <42sh.h>

int	is_special_car(char c)
{
  if (c == '\\' ||
      c == 'a' ||
      c == 'b' ||
      c == 'c' ||
      c == 'e' ||
      c == 'f' ||
      c == 'n' ||
      c == 'r' ||
      c == 't' ||
      c == 'v' ||
      c == '0')
    return (1);
  return (0);
}

char	get_octal_char(char *str, int *i)
{
  char	nb[5];
  int	j;

  (*i)--;
  j = 0;
  while (str[++(*i)] >= 48 && str[(*i)] <= 55 && j < 4)
    nb[j++] = str[*i];
  if (str[(*i)] < 48 || str[(*i)] > 55)
    (*i)--;
  nb[j] = 0;
  return ((my_getnbr_base(nb, "01234567")));
}

char	get_special_car(char *str, int *i, int *stop)
{
  (*i)++;
  if (str[*i] == '\\')
    return ('\\');
  if (str[*i] == 'a')
    return ('\a');
  if (str[*i] == 'b')
    return ('\b');
  if (str[*i] == 'c')
    return ((*stop = 1));
  if (str[*i] == 'e')
    return (27);
  if (str[*i] == 'f')
    return ('\f');
  if (str[*i] == 'n')
    return ('\n');
  if (str[*i] == 'r')
    return ('\r');
  if (str[*i] == 't')
    return ('\t');
  if (str[*i] == 'v')
    return ('\v');
  return (get_octal_char(str, i));
}

int	print_str(char *str)
{
  int	i;
  char	c;
  int	stop;

  i = -1;
  while (str[++i])
    {
      stop = 0;
      if (str[i] == '\\' && is_special_car(str[i + 1]))
	c = get_special_car(str, &i, &stop);
      else
	c = str[i];
      if (stop)
	return (1);
      my_printf("%c", c);
    }
  return (0);
}

int	do_echo(char ***prog)
{
  int	final;
  int	i;

  final = 1;
  i = 0;
  if ((*prog)[1] == NULL)
    {
      my_printf("\n");
      return (0);
    }
  while ((*prog)[++i])
    {
      if (i == 1 && !my_strcmp((*prog)[i], "-n"))
	final = 0;
      else
	if (print_str((*prog)[i]))
	  return (0);
      if (i < my_tablen((*prog)) - 1
	  && (i != 1 || my_strcmp((*prog)[i], "-n") != 0))
	my_printf(" ");
      else if (final)
	my_printf("\n");
    }
  return (0);
}
