/*
** rc_reader.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Fri Jun  3 17:12:28 2016 Matthias RIGAUD
** Last update Sun Jun  5 22:41:10 2016 Michel Mancier
*/

# include <42sh.h>

char	*get_file(char **env)
{
  int	pos;
  char	*str;

  if ((pos = find_line(env, "HOME")) == -1)
    {
      if ((pos = find_line(env, "USER")) == -1)
	return (my_strdup("/home/" DEFAULT_FILE));
      str = xmalloc(my_strlen("/home/" DEFAULT_FILE)
		    + my_strlen(&env[pos][5]) + 2);
      my_strcpy(str, "/home/");
      my_strcat(str, &env[pos][5]);
      my_strcat(str, "/");
      my_strcat(str, DEFAULT_FILE);
      return (str);
    }
  str = xmalloc(my_strlen(&env[pos][5]) + my_strlen(DEFAULT_FILE) + 2);
  my_strcpy(str, &env[pos][5]);
  my_strcat(str, "/");
  my_strcat(str, DEFAULT_FILE);
  return (str);
}

void	write_fd(int fd, char *str)
{
  write(fd, str, my_strlen(str));
}

void	rc_creator(char *file)
{
  int	fd;

  if ((fd = open(file, O_CREAT | O_WRONLY | O_TRUNC, 0644)) == -1)
    {
      fprintf(stderr, "Open : %s.\nCan't access %s\n", strerror(errno), file);
      return ;
    }
  write_fd(fd, "# Welcolme to the true .42shrc !\n");
  write_fd(fd, "# 42sh, the alternative shell !\n");
  write_fd(fd, "# create by raposo_c, girold_a, allary_a,"
	   " rigaud_b and mancie_m\n\n");
  write_fd(fd, "# setenv PS1 \"[$USER@$HOSTNAME] $>\"\n");
  write_fd(fd, "# setenv PATH \"/bin:/sbin:/usr/bin:/usr/sbin:"
	   "/usr/heimdal/bin:/usr/heimdal/sbin\"\n");
  write_fd(fd, "# setenv LD_LIBRARY_PATH \"/home/{$HOME}/.froot/lib/\"\n");
  write_fd(fd, "# setenv C_INCLUDE_PATH \"/home/{$HOME}/.froot/include/\"\n");
  write_fd(fd, "# setenv CPLUS_INCLUDE_PATH \"/home/{$HOME}/.froot/include/\"\n");
  close(fd);
}

void	rc_reader(char ***env)
{
  int	fd;
  char	*file;
  char	*line;
  char	**prog;

  if (!(file = get_file(*env)))
    my_puterror("Error Malloc\n", 42);
  if ((fd = open(file, O_RDONLY)) == -1)
    rc_creator(file);
  if ((fd = open(file, O_RDONLY)) == -1)
    return ;
  while ((line = get_next_line(fd, 0)))
    {
      if (line[0] == '#' || !line[0])
	free(line);
      else
	prep_exec(env, &line, &prog, 0);
    }
  close(fd);
  free(file);
}
