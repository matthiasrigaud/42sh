/*
** get_next_line.c for mysh in /home/rigaud_b/rendu/PSU_2015_minishell2/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Thu Mar 31 18:06:06 2016 Matthias RIGAUD
** Last update Wed Jun  1 16:17:38 2016 clement raposo
*/

#include <get_next_line.h>

int	purge_buffer(t_buf *buf)
{
  char	*new_buf;
  int	i;
  int	j;

  if (!(new_buf = malloc(READ_SIZE + 1)))
    return (1);
  i = is_10_in(buf);
  j = i + 1;
  while (++i < buf->size)
    new_buf[i - j] = buf->str[i];
  buf->size = i - j;
  free(buf->str);
  buf->str = new_buf;
  return (0);
}

int	add_n_car(t_buf *dest, t_buf *src, int n)
{
  char  *ndest;
  int   i;

  if (n < 0)
    n = src->size;
  if (!(ndest = malloc(dest->size + n + 1)))
    return (1);
  i = -1;
  while (++i < dest->size)
    ndest[i] = dest->str[i];
  free(dest->str);
  dest->str = ndest;
  i = -1;
  while (++i < n)
    {
      if (i >= src->size)
	{
	  dest->size += src->size;
	  return (0);
	}
      dest->str[dest->size + i] = src->str[i];
    }
  dest->size += n;
  return (0);
}

int     is_10_in(t_buf *buf)
{
  int   i;

  i = -1;
  while (++i < buf->size)
    {
      if (buf->str[i] == 10)
	return (i);
    }
  return (-1);
}

int	reader(t_buf *buffer, t_buf *line, const int fd)
{
  if (add_n_car(line, buffer, buffer->size))
    return (1);
  if ((buffer->size = read(fd, buffer->str, READ_SIZE)) < 0)
    return (1);
  if (!buffer->size && !line->size)
    return (1);
  if (!buffer->size)
    return (2);
  return (0);
}

char		*get_next_line(const int fd, int erase)
{
  static t_buf	buffer;
  t_buf		line;
  int		ret;

  if (free_buffer(erase, &buffer) == 0 || !(line.str = malloc(1)))
    return (NULL);
  line.size = 0;
  if (!buffer.str || erase)
    {
      if (!(buffer.str = malloc(READ_SIZE + 1)))
	return (NULL);
      buffer.size = 0;
    }
  while (is_10_in(&buffer) < 0)
    if ((ret = reader(&buffer, &line, fd)) == 1)
      return (NULL);
    else if (ret == 2)
      break;
  if (add_n_car(&line, &buffer, is_10_in(&buffer)))
    return (NULL);
  if (purge_buffer(&buffer))
    return (NULL);
  line.str[line.size] = 0;
  return (line.str);
}
