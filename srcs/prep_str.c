/*
** prep_str.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:43:45 2016 Matthias RIGAUD
** Last update Sat Jun  4 20:07:25 2016 Matthias RIGAUD
*/

# include <42sh.h>

int		is_spe_car(char c, int can_do_that)
{
  static int	quote;

  if (c == '"' && can_do_that == 1)
    quote = (quote + 1) % 2;
  if ((c == '&' ||
       c == '|' ||
       c == ';' ||
       c == '>' ||
       c == '<') && !((quote + (can_do_that < -1 ? 1 : 0)) % 2))
    return (1);
  return (0);
}

int	nb_car(char *str)
{
  int	i;
  int	nb;

  nb = 0;
  i = -1;
  while (str[++i])
    if (is_spe_car(str[i], 1))
      nb++;
  if (!is_spe_car(';', 1))
    is_spe_car('"', 1);
  return (nb);
}

void	prep_str(char	**str, t_als *alias)
{
  int	i;
  int	j;
  char	*new;

  replace_alias(str, alias, 3);
  new = xmalloc(my_strlen(*str) + 1 + 2 * nb_car(*str));
  i = -1;
  j = 0;
  while ((*str)[++i])
    if ((is_spe_car((*str)[i], 1) && (!i || !is_spe_car((*str)[i - 1], -1)
				      || (*str)[i - 1] != (*str)[i])) ||
	(i && is_spe_car((*str)[i - 1], -1) && !is_spe_car((*str)[i], 0)))
      {
	new[i + j++] = ' ';
	new[i + j] = (*str)[i];
      }
    else
      new[i + j] = (*str)[i];
  if (!is_spe_car(';', 1))
    is_spe_car('"', 1);
  new[i + j] = 0;
  free(*str);
  *str = new;
}
