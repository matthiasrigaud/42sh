/*
** redirect_in.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:44:05 2016 Matthias RIGAUD
** Last update Thu Jun  2 11:40:08 2016 Matthias RIGAUD
*/

# include <42sh.h>

int	make_double_in(char *end)
{
  char	*line;
  int	fd;

  signal(SIGINT, SIG_IGN);
  if ((fd = open("/tmp/tmp.42sh", O_CREAT | O_WRONLY | O_TRUNC, 0644)) == -1)
    {
      fprintf(stderr, "Open Error: %s.\n", strerror(errno));
      exit(1);
    }
  while (my_printf("? ") && (line = get_next_line(0, 0)) &&
	 my_strcmp(line, end))
    {
      write(fd, line, my_strlen(line));
      write(fd, "\n", 1);
      free(line);
    }
  close(fd);
  if ((fd = open("/tmp/tmp.42sh", O_RDONLY)) == -1)
    {
      fprintf(stderr, "Open Error: %s.\n", strerror(errno));
      exit(1);
    }
  signal(SIGINT, ctrl_c);
  return (fd);
}

int	get_fd_in(int *fd, char **argv, int pos_chev, int *n)
{
  if (!my_strncmp(argv[pos_chev], "<", 1) && argv[pos_chev][1] != '<'
      && (*n = 1))
    {
      if (my_strlen(argv[pos_chev]) > 1)
	{
	  if (((*fd = open(&argv[pos_chev][1], O_RDONLY)) == -1))
	    return (1);
	}
      else if ((*n = 2))
	if (((*fd = open(argv[pos_chev + 1], O_RDONLY)) == -1))
	  return (1);
    }
  else
    if (my_strlen(argv[pos_chev]) > 2 && (*n = 1))
      *fd = make_double_in(&argv[pos_chev][2]);
    else if ((*n = 2))
      *fd = make_double_in(argv[pos_chev + 1]);
  return (0);
}

int	is_redirect_in(char ***av, int *fd)
{
  int	i;
  int	nb_to_dl;
  int	pos;
  int	redirect;

  if (*fd != -1)
    return (-1);
  redirect = 0;
  i = -1;
  while ((*av)[++i])
    if (!my_strncmp((*av)[i], "<", 1) && !redirect)
      {
	redirect++;
	pos = i;
      }
    else if (!my_strncmp((*av)[i], "<", 1))
      return (-2);
  if (redirect && (!(*av)[pos + 1] && (!my_strcmp((*av)[pos], "<<")
					   || !my_strcmp((*av)[pos], "<"))))
    return (-2);
  if (redirect == 1 && !get_fd_in(fd, *av, pos, &nb_to_dl) && *fd >= 0)
    my_tab_comp(pos, nb_to_dl, av);
  else if (redirect == 1)
    return (-2);
  return (*fd);
}

int		set_redirect_in(int new_fd, int old_fd)
{
  static int	fd_stdin = -1;

  if (fd_stdin == -1)
    if ((fd_stdin = dup(0)) == -1)
      my_puterror("Error dup\n", 42);
  if (new_fd == -2)
    return (1);
  if (new_fd >= 0 && old_fd >= 0)
    {
      if (old_fd == 0)
	old_fd = fd_stdin;
      if (dup2(old_fd, new_fd) == -1)
	my_puterror("Error redirect\n", 42);
    }
  return (0);
}
