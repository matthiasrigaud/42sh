/*
** alias_list.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Sat Jun  4 01:34:46 2016 Matthias RIGAUD
** Last update Sat Jun  4 01:49:04 2016 Matthias RIGAUD
*/

# include <42sh.h>

void	free_elem(t_als *elem)
{
  free(elem->old);
  free(elem->alias);
  free(elem);
}

void	delete_elem(t_als **alias, char *str)
{
  t_als	*tmp;
  t_als	*ttmp;

  tmp = *alias;
  if (!(ttmp = NULL) && tmp)
    {
      if (!my_strcmp(tmp->old, str))
	{
	  *alias = tmp->next;
	  free_elem(tmp);
	  return ;
	}
      ttmp = tmp->next;
    }
  while (ttmp)
    {
      if (!my_strcmp(ttmp->old, str))
	{
	  tmp->next = ttmp->next;
	  free_elem(ttmp);
	  return ;
	}
      tmp = ttmp;
      ttmp = ttmp->next;
    }
}

void	add_elem(t_als **alias, char *old, char *new)
{
  t_als	*tmp;

  tmp = xmalloc(sizeof(t_als));
  if (!(tmp->old = my_strdup(old)))
    my_puterror("Error Malloc\n", 42);
  if (!(tmp->alias = my_strdup(new)))
    my_puterror("Error Malloc\n", 42);
  tmp->next = *alias;
  *alias = tmp;
}

void	disp_list(t_als *alias)
{
  if (!alias)
    return ;
  my_printf("%s\t%s\n", alias->old, alias->alias);
  disp_list(alias->next);
}
