/*
** cd.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:41:11 2016 Matthias RIGAUD
** Last update Sun Jun  5 16:01:33 2016 Matthias RIGAUD
*/

# include <42sh.h>

void	tab_cat(char ***tab, char **str)
{
  char	**nw_tab;
  int	i;

  if (!(nw_tab = malloc((my_tablen(*tab) + 2) * sizeof(char *))))
      my_puterror("Memory error\n", 1);
  i= -1;
  while ((*tab)[++i])
    nw_tab[i] = (*tab)[i];
  nw_tab[i] = (*str);
  nw_tab[i + 1] = NULL;
  (*tab) = nw_tab;
}

void	set_pwd(char ***env)
{
  char	*pwd;
  char	*oldpwd;
  int	pwd_i;
  int	old_i;

  pwd = xmalloc(100);
  pwd = my_strcpy(pwd, "PWD=");
  getcwd(&pwd[4], 95);
  pwd_i = find_line(*env, "PWD");
  old_i = find_line(*env, "OLDPWD");
  if (pwd_i < 0)
    tab_cat(env, &pwd);
  else
    {
      oldpwd = xmalloc(4 + my_strlen((*env)[pwd_i]));
      oldpwd = my_strcpy(oldpwd, "OLDPWD=");
      oldpwd = my_strcat(oldpwd, &(*env)[pwd_i][4]);
      (*env)[pwd_i] = pwd;
      if (old_i < 0)
	tab_cat(env, &oldpwd);
      else
	(*env)[old_i] = oldpwd;
    }
}

int	cd_error(char *path)
{
  int	cookie;

  fprintf(stderr, "%s: %s.\n", path, strerror(errno));
  cookie = 256;
  must_exit(NULL, &cookie, 0);
  return (1);
}

int	cd(char **com, char *home, char ***env)
{
  int	ret;

  if (my_tablen(com) > 2)
    my_printf("Too many arguments.\n");
  else if (my_tablen(com) < 2 || (com[1][0] == '~' && my_strlen(com[1]) < 3))
    ret = chdir(home);
  else if (com[1][0] == '-' && my_strlen(com[1]) == 1)
    {
      ret = find_line(*env, "OLDPWD");
      ret = (ret < 0 ? 0 : chdir(&(*env)[ret][7]));
    }
  else
    {
      if (com[1][0] == '~')
	{
	  if (!(ret = chdir(home)))
	    ret = chdir(&com[1][2]);
	}
      else
	ret = chdir(com[1]);
    }
  set_pwd(env);
  if (ret)
    return (cd_error(com[1]));
  return (0);
}
