/*
** builtin2.c for Friends in /home/mei/Projets/PSU_2015_42sh/srcs
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.net>
**
** Started on  Fri Jun  3 17:20:51 2016 Michel Mancier
** Last update Sat Jun  4 02:39:31 2016 Matthias RIGAUD
*/

#include "42sh.h"

void	built_echo(char ***env, char ***prog, int *end, t_als **alias)
{
  (void)env;
  (void)alias;
  *end = do_echo(prog);
}

void	built_alias(char ***env, char ***prog, int *end, t_als **alias)
{
  (void)env;
  *end = make_alias(prog, alias);
}

void	built_exec(char ***env, char ***prog, int *end, t_als **alias)
{
  (void)alias;
  *end = my_exec(find_commande((*prog)[0], *env), *prog, *env);
}

int	which_builtin(char *cmd)
{
  if (!my_strcmp("cd", cmd))
    return (0);
  else if (!my_strcmp("env", cmd))
    return (1);
  else if (!my_strcmp("setenv", cmd))
    return (2);
  else if (!my_strcmp("unsetenv", cmd))
    return (3);
   else if (!my_strcmp("echo", cmd))
      return (4);
  else if (!my_strcmp("alias", cmd) || !my_strcmp("unalias", cmd))
    return (5);
  else
    return (6);
}
