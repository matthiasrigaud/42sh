/*
** prompt.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Thu Jun  2 21:53:25 2016 Matthias RIGAUD
** Last update Thu Jun  2 22:07:53 2016 Matthias RIGAUD
*/

# include <42sh.h>

void		disp_prompt(char **env)
{
  static char	*prompt;
  int		pos;

  if (!(env) && prompt)
    {
      free(prompt);
      return ;
    }
  if (!prompt)
    {
      if ((pos = find_line(env, PROMPT_VAR)) >= 0)
	prompt = my_strdup(&env[pos][my_strlen(PROMPT_VAR) + 1]);
      else
	prompt = my_strdup(DEFAULT_PROMPT);
    }
  my_printf("%s", prompt);
}
