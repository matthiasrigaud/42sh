/*
** replace_alias.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Sat Jun  4 05:54:09 2016 Matthias RIGAUD
** Last update Sat Jun  4 19:51:46 2016 Matthias RIGAUD
*/

# include <42sh.h>

char	last_char(char *str, int pos)
{
  int	i;

  i = pos;
  while (--i >= 0)
    if (str[i] != 32 && str[i] != '\t' && str[i] != '\v')
      return (str[i]);
  return ('|');
}

int	find_alias_on_str(char *str, char *alias, int pos)
{
  int	i;
  int	j;

  i = pos - 1;
  j = 0;
  while (str[++i] && (is_spe_car(str[i], 1)) >= 0)
    if (alias[j] == str[i])
      {
	if (!alias[++j])
	  {
	    if (is_spe_car(last_char(str, i - j + 1), 0))
	      return (i - j + 1);
	    i -= j - 1;
	    j = 0;
	  }
      }
    else
      {
	i -= j;
	j = 0;
      }
  return (-1);
}

static void	replace_str(char **str, char *insert, int pos, int size)
{
  char		*new;
  int		i;

  new = xmalloc(my_strlen(insert) + my_strlen(*str) - size + 1);
  i = -1;
  while (++i < my_strlen(insert) + my_strlen(*str) - size)
    {
      if (i < pos)
	new[i] = (*str)[i];
      else if (i - pos < my_strlen(insert))
	new[i] = insert[i - pos];
      else
	new[i] = (*str)[i - my_strlen(insert) + size];
    }
  new[i] = 0;
  free(*str);
  *str = new;
}

void	replace_alias(char **str, t_als *alias, int level)
{
  t_als	*tmp;
  char	*replace;
  int	pos;

  if (!level)
    return ;
  tmp = alias;
  while (tmp)
    {
      pos = 0;
      while ((pos = find_alias_on_str(*str, tmp->old, pos)) != -1)
	{
	  if (!(replace = my_strdup(tmp->alias)))
	    my_puterror("Error Malloc\n", 42);
	  replace_alias(&replace, alias, level - 1);
	  replace_str(str, replace, pos, my_strlen(tmp->old));
	  pos += my_strlen(replace);
	  free(replace);
	}
      tmp = tmp->next;
    }
}
