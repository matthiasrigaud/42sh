/*
** div.c for 42s in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:41:45 2016 Matthias RIGAUD
** Last update Sat Jun  4 00:57:54 2016 Matthias RIGAUD
*/

# include <42sh.h>

void	*xmalloc(int size)
{
  void	*returner;

  if (!(returner = malloc(size)))
    my_puterror("Memory error\n", 1);
  return (returner);
}

void	my_close(int fd)
{
  if (fd >= 0)
    close(fd);
}

int	char_is_in_str(char *str, char c)
{
  int	i;

  if (!str && ((c >= 65 && c <= 90) ||
	       (c >= 48 && c <= 57) ||
	       (c >= 97 && c <= 122) ||
	       c == 95 || c == 63))
    return (0);
  else if (!str)
    return (1);
  i = -1;
  while (str[++i])
    if (str[i] == c)
      return (1);
  return (0);
}
