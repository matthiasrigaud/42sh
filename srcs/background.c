/*
** background.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Sun Jun  5 00:41:52 2016 Matthias RIGAUD
** Last update Sun Jun  5 00:42:35 2016 Matthias RIGAUD
*/

# include <42sh.h>

void	end_back(int end, char **prog)
{
  if (!end)
    my_printf("[x]\tDone\t\t\t\t");
  else
    my_printf("[x]\tExit %i\t\t\t\t", end);
  my_show_wordtab(prog);
  exit(1);
}
