/*
** command.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:41:25 2016 Matthias RIGAUD
** Last update Sat Jun  4 04:57:06 2016 Matthias RIGAUD
*/

# include <42sh.h>

int	find_line(char **env, char *field)
{
  int	i;

  i = -1;
  while (env[++i])
    if (my_strlen(env[i]) >= my_strlen(field) + 1)
      if (my_strcmp(field, env[i]) == -61 && env[i][my_strlen(field)] == 61)
	return (i);
  return (-1);
}

char	*prepare_str(char *str)
{
  int	i;
  char	*path;

  path = xmalloc(my_strlen(str) + 1);
  i = -1;
  while (str[++i])
    if (str[i] == ':')
      path[i] = 0;
    else
      path[i] = str[i];
  path[i] = 0;
  return (path);
}

void	absolut_path(char *path, char *exe, char **ex_path)
{
  int	i;
  int	j;

  *ex_path = xmalloc(my_strlen(path) + my_strlen(exe) + 2);
  i = -1;
  while (path[++i])
    (*ex_path)[i] = path[i];
  (*ex_path)[i] = '/';
  i++;
  j = -1;
  while (exe[++j])
    (*ex_path)[i + j] = exe[j];
  (*ex_path)[i + j] = 0;
}

int	is_slash_on(char *str)
{
  int	i;

  i = -1;
  if (!str)
    return (1);
  while (str[++i])
    if (str[i] ==  '/')
      return (1);
  return (0);
}

char	*find_commande(char *prog, char **env)
{
  char	*path;
  char	*exe;
  int	pos;
  int	i;

  if (is_slash_on(prog))
    return (prog);
  if ((pos = find_line(env, "PATH")) != -1)
    path = prepare_str(env[pos]);
  else
    path = prepare_str(DEFAULT_PATH);
  i = 5;
  while (i < my_strlen((pos == -1 ? DEFAULT_PATH : env[pos])))
    {
      absolut_path(&path[i], prog, &exe);
      if (!access(exe, F_OK))
	{
	  free(path);
	  return (exe);
	}
      i += my_strlen(&path[i]) + 1;
    }
  free(path);
  return (prog);
}
