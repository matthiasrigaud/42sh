/*
** redirect_out.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:44:18 2016 Matthias RIGAUD
** Last update Wed Jun  1 14:44:23 2016 Matthias RIGAUD
*/

# include <42sh.h>

void	get_fd_out(int *fd, char **argv, int pos_chev, int *n)
{
  if (!my_strncmp(argv[pos_chev], ">", 1) && argv[pos_chev][1] != '>'
      && (*n = 1))
    {
      if (my_strlen(argv[pos_chev]) > 1)
	{
	  if (((*fd = open(&argv[pos_chev][1], O_CREAT |
			   O_WRONLY | O_TRUNC, 0644)) == -1))
	    my_puterror("Open Error\n", 42);
	}
      else if ((*n = 2))
	if (((*fd = open(argv[pos_chev + 1], O_CREAT |
			 O_WRONLY | O_TRUNC, 0644)) == -1))
	  my_puterror("Open Error\n", 42);
    }
  else
    if (my_strlen(argv[pos_chev]) > 2 && (*n = 1))
      {
	if (((*fd = open(&argv[pos_chev][2], O_CREAT |
			 O_WRONLY | O_APPEND, 0644)) == -1))
	  my_puterror("Open Error\n", 42);
      }
    else if ((*n = 2))
      if (((*fd = open(argv[pos_chev + 1], O_CREAT |
		       O_WRONLY | O_APPEND, 0644)) == -1))
	my_puterror("Open Error\n", 42);
}

int	is_redirect_out(char ***av, int *fd)
{
  int	i;
  int	nb_to_dl;
  int	pos;
  int	redirect;

  redirect = 0;
  i = -1;
  if (*fd != -1)
    return (-1);
  while ((*av)[++i])
    if (!my_strncmp((*av)[i], ">", 1) && !redirect)
      {
	redirect++;
	pos = i;
      }
    else if (!my_strncmp((*av)[i], ">", 1))
      return (-2);
  if (redirect && (!(*av)[pos + 1] && (!my_strcmp((*av)[pos], ">>")
					   || !my_strcmp((*av)[pos], ">"))))
    return (-2);
  if (redirect == 1)
    get_fd_out(fd, *av, pos, &nb_to_dl);
  if (*fd >= 0)
    my_tab_comp(pos, nb_to_dl, av);
  return (*fd);
}

int		set_redirect_out(int new_fd, int old_fd)
{
  static int	fd_stdout = -1;

  if (fd_stdout == -1)
    if ((fd_stdout = dup(1)) == -1)
      my_puterror("Error dup\n", 42);
  if (new_fd == -2)
    return (1);
  if (new_fd >= 0 && old_fd >= 0)
    {
      if (old_fd == 1)
	old_fd = fd_stdout;
      if (dup2(old_fd, new_fd) == -1)
	my_puterror("Error redirect\n", 42);
    }
  return (0);
}
