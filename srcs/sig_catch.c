/*
** sig_catch.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:44:34 2016 Matthias RIGAUD
** Last update Wed Jun  1 14:44:38 2016 Matthias RIGAUD
*/

# include <42sh.h>

int		is_ctrl_c(int sig)
{
  static int	i;

  if (!sig && i)
    {
      i = 0;
      return (1);
    }
  if (sig)
    i = 1;
  return (0);
}

void	ctrl_c(int sig)
{
  (void)sig;
  my_printf("\n");
  is_ctrl_c(1);
  signal(SIGINT, ctrl_c);
}
