/*
** free_buffer.c for 42sh in /home/raposo_c/rendu/PSU/PSU_2015_42sh
** 
** Made by clement raposo
** Login   <raposo_c@epitech.net>
** 
** Started on  Wed Jun  1 16:16:54 2016 clement raposo
** Last update Wed Jun  1 16:19:22 2016 clement raposo
*/

#include <get_next_line.h>

int		free_buffer(int erase, t_buf *buffer)
{
  if (erase == -1 && buffer->str != NULL)
    {
      free(buffer->str);
      return (0);
    }
  return (1);
}
