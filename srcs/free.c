/*
** free.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Thu Jun  2 22:19:20 2016 Matthias RIGAUD
** Last update Fri Jun  3 20:12:42 2016 Matthias RIGAUD
*/

# include <42sh.h>

void	final_free(void)
{
  get_next_line(0, -1);
  disp_prompt(NULL);
  my_printf("exit\n");
}
