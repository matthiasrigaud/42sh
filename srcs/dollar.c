/*
** dollar.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Thu Jun  2 12:13:12 2016 Matthias RIGAUD
** Last update Sat Jun  4 05:58:00 2016 Matthias RIGAUD
*/

# include <42sh.h>

int	get_next_char(char *str, char *c, int pos)
{
  int	i;

  i = pos - 1;
  while (str[++i])
    if (char_is_in_str(c, str[i]))
      return (i);
  return (i);
}

char	*get_next_word(char *str, char *c)
{
  char	*word;
  int	next;

  next = get_next_char(str, c, 0);
  word = xmalloc(next * sizeof(char) + 1);
  my_strncpy(word, str, next);
  word[next] = 0;
  return (word);
}

static void	replace_str(char **str, char *insert, int pos, char *c)
{
  char	*new;
  int	size;
  int	i;

  size = get_next_char((*str), c, pos + 1) - pos;
  new = xmalloc(my_strlen(insert) + my_strlen(*str) - size + 1);
  i = -1;
  while (++i < my_strlen(insert) + my_strlen(*str) - size)
    {
      if (i < pos)
	new[i] = (*str)[i];
      else if (i - pos < my_strlen(insert))
	new[i] = insert[i - pos];
      else
	new[i] = (*str)[i - my_strlen(insert) + size];
    }
  new[i] = 0;
  free(*str);
  *str = new;
}

char	*nb_to_str(char *out, int nb)
{
  int	i;
  int	pow;

  pow = 10;
  while (nb > pow)
    pow *= 10;
  pow /= 10;
  i = 0;
  while (pow)
    {
      out[i++] = nb / pow + 48;
      nb %= pow;
      pow /= 10;
    }
  out[i] = 0;
  return (out);
}

int	replace_dollar(char **env, char ***prog, int end)
{
  int	i;
  int	next;
  int	pos;
  char	*var;
  char	end_str[10];

  i = -1;
  while ((*prog)[++i])
    {
      next = -1;
      while ((next = get_next_char((*prog)[i], "$", next + 1))
	     < my_strlen((*prog)[i]))
	{
	  var = get_next_word(&(*prog)[i][next + 1], NULL);
	  if (my_strlen(var) > 0)
	    {
	      if ((pos = find_line(env, var)) < 0 && my_strcmp(var, "?"))
		return (fprintf(stderr, "%s: Undefined variable.\n", var));
	      replace_str(&(*prog)[i], (pos >= 0 ? &env[pos][my_strlen(var) + 1]
					: nb_to_str(end_str, end)), next, NULL);
	    }
	  free(var);
	}
    }
  return (0);
}
