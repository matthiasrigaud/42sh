/*
** builtin.c for Friends in /home/mei/Projets/PSU_2015_42sh/srcs
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.net>
**
** Started on  Fri Jun  3 15:55:32 2016 Michel Mancier
** Last update Sun Jun  5 15:44:35 2016 Matthias RIGAUD
*/

#include "42sh.h"

void	built_cd(char ***env, char ***prog, int *end, t_als **alias)
{
  (void)prog;
  (void)alias;
  *end = cd(*prog, &(*env)[find_line(*env, "HOME")][5], env);
}

void	built_env(char ***env, char ***prog, int *end, t_als **alias)
{
  (void)prog;
  (void)alias;
  *end = do_env(*env);
}

void	built_setenv(char ***env, char ***prog, int *end, t_als **alias)
{
  (void)alias;
  *end = setenv(env, *prog);
  must_exit(NULL, end, 0);
}

void	built_unsetenv(char ***env, char ***prog, int *end, t_als **alias)
{
  (void)alias;
  *end = unsetenv(env, *prog);
}

void	create_builtins(void (*built[8])(char ***, char ***, int *, t_als **))
{
  built[0] = &built_cd;
  built[1] = &built_env;
  built[2] = &built_setenv;
  built[3] = &built_unsetenv;
  built[4] = &built_echo;
  built[5] = &built_alias;
  built[6] = &built_exec;
  built[7] = NULL;
}
