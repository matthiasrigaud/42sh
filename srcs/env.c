/*
** env.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Wed Jun  1 14:42:03 2016 Matthias RIGAUD
** Last update Wed Jun  8 14:14:22 2016 Matthias RIGAUD
*/

# include <42sh.h>

int	do_env(char **env)
{
  int	i;

  i = -1;
  while (env[++i])
    my_printf("%s\n", env[i]);
  return (0);
}

int	char_pah_var(char c)
{
  if ((c >= 65 && c <= 90) ||
      (c >= 48 && c <= 57) ||
      (c >= 97 && c <= 122) ||
      c == 95)
    return (1);
  return (0);
}

int	unsetenv(char ***env, char **com)
{
  int	i;
  int	j;

  if (my_tablen(com) == 1)
    {
      my_printf("Too few arguments\n");
      return (1);
    }
  j = 0;
  while (com[++j])
    {
      if ((i = find_line(*env, com[j])) != -1)
	{
	  while ((*env)[++i])
	    (*env)[i - 1] = (*env)[i];
	  (*env)[i - 1] = NULL;
	}
    }
  return (0);
}

int	setenv(char ***env, char **com)
{
  int	i;
  char	*var;

  if (my_tablen(com) != 3 && my_tablen(com) != 2)
    return ((my_tablen(com) == 1) ? (do_env(*env)) : (my_tablen(com) > 3) ?
	    (my_puterror("Too many args\n", 0)) : (1));
  else if ((i = -1))
    {
      while (com[1][++i])
	if (!char_pah_var(com[1][i]))
	  return (fprintf(stderr, "setenv: Variable name must "
			  "contain alphanumeric characters.\n"), 1);
      var = xmalloc(my_strlen(com[1]) + my_strlen((com[2]) ? (com[2])
						  : ("")) + 2);
      var = my_strcpy(var, com[1]);
      var = my_strcat(var, "=");
      var = my_strcat(var, (com[2]) ? (com[2]) : (""));
      if ((i = find_line(*env, com[1])) == -1)
	tab_cat(env, &var);
      else
	(*env)[i] = var;
    }
  return (0);
}
