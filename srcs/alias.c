/*
** alias.c for 42sh in /home/rigaud_b/rendu/PSU_2015_42sh/srcs
**
** Made by Matthias RIGAUD
** Login   <rigaud_b@epitech.eu>
**
** Started on  Fri Jun  3 23:52:58 2016 Matthias RIGAUD
** Last update Sat Jun  4 05:54:28 2016 Matthias RIGAUD
*/

# include <42sh.h>

int	unalias(char ***prog, t_als **alias)
{
  int	i;

  if (my_tablen((*prog)) < 2)
    {
      fprintf(stderr, "unalias: Too few arguments.\n");
      return (1);
    }
  i = 0;
  while ((*prog)[++i])
    delete_elem(alias, (*prog)[i]);
  return (0);
}

void	disp_alias(t_als *alias, char *old)
{
  t_als	*tmp;

  tmp = alias;
  while (tmp)
    {
      if (!my_strcmp(old, tmp->old))
	my_printf("%s\n", tmp->alias);
      tmp = tmp->next;
    }
}

int	make_alias(char ***prog, t_als **alias)
{
  if (!my_strcmp((*prog)[0], "unalias"))
    return (unalias(prog, alias));
  if (my_tablen((*prog)) == 1)
    disp_list(*alias);
  if (my_tablen((*prog)) == 2)
    disp_alias(*alias, (*prog)[1]);
  if (my_tablen((*prog)) < 3)
    return (0);
  delete_elem(alias, (*prog)[1]);
  add_elem(alias, (*prog)[1], (*prog)[2]);
  return (0);
}
