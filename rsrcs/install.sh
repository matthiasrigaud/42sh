#!/bin/sh
sudo echo -e "(add-to-list 'load-path \"emacs.d/\")" >> ~/.emacs
sudo echo -e "(load \"42shrc.el\")" >> ~/.emacs
sudo cp 42shrc.el ~/.emacs.d/
sudo cp 42sh.1.gz /usr/share/man/man1/
sudo cp ../42sh /usr/bin/
sudo ln -s /usr/bin/42sh ./saltsh
sudo mv saltsh /usr/bin/
