;;
;; 42shrc.el for 42shrc in /home/mei/.emacs.d
;; 
;; Made by Michel Mancier
;; Login   <mei@epitech.net>
;; 
;; Started on  Fri Jun  3 20:16:29 2016 Michel Mancier
;; Last update Fri Jun  3 21:38:25 2016 Michel Mancier
;;
;; With that Emacs-Lisp script, you will be able to get
;; colored .42shrc even though it is not an official file.
;;

(defvar 42shrc-mode-hook nil)

;; Autoloads the .42shrc as a 42sh file.
(add-to-list 'auto-mode-alist '("\\.42shrc" . 42shrc-mode))

(defconst 42shrc-font-lock-keywords-1
  (list
   '("\\<\\(setenv\\|unsetenv\\|alias\\|cd\\)\\>" . font-lock-builtin-face)
   '("\\<\\(PS1\\)\\>" . font-lock-constant-face))
  "Minimal highlighting expressions for .42shrc mode")

(defvar 42shrc-font-lock-keywords 42shrc-font-lock-keywords-1)

;; Syntax table : Colorer ces commentaires

(defvar 42shrc-mode-syntax-table nil)
(setq 42shrc-mode-syntax-table
      (let ((synTable (make-syntax-table)))
	;; Commentaires en "#..."
	(modify-syntax-entry ?# "< b" synTable)
	(modify-syntax-entry ?\n "> b" synTable)

	synTable))

(defun 42shrc-mode ()
  "Major mode for editing .42shrc. Epitech 2016"
  (interactive)
  (kill-all-local-variables)
  (set-syntax-table 42shrc-mode-syntax-table)
  (set (make-local-variable 'font-lock-defaults) '(42shrc-font-lock-keywords))
  (setq major-mode '42shrc-mode)
  (setq mode-name "42shrc")
  (setq-local comment-start "# ")
  (setq-local comment-end "")
  (run-hooks '42shrc-mode-hook))

(provide '42shrc-mode)

